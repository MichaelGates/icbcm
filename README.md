# IcbcM


## Tencent Meeting

会议主题：繁星计划-敏捷精益
会议时间：2023/05/18 16:00-18:00 (GMT+08:00) 中国标准时间 - 澳门

点击链接入会，或添加至会议列表：
https://meeting.tencent.com/dm/c6Ei5sUXk7iY

腾讯会议：269-412-055
会议密码：122333

## Material

- [分手厨房](https://www.youtube.com/watch?v=R5GCP6V94Lo)
- [迷惑的广东话](https://www.youtube.com/watch?v=xdQBKdw1_Cg)

## Countdown

- [倒数计数器](https://tw.piliapp.com/timer/countdown/)
- [计时器](https://naozhong.net.cn/jishiqi/)

## Feedback

- [ ] [Feedback](https://wall.sli.do/event/xhqW6pzcayEMq5xZB2uo2W)
